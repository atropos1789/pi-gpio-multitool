# Raspberry Pi GPIO Multitool

## Project Specification

I want a raspberry pi zero that has the following properties
- i2c, spi, and other protocols have all of the necessary kernel modules and userspace tools installed
- the pi can be connected to using a single usb connection
- the pi can be accessed with passworldless ssh login
- the pi has a minimal headless development environment that mirrors the terminal tools I use on my laptop


## Project Background

I am interested in gpio programming at a low level, so using the C kernel interfaces instead of the gpiozero python ones created by Raspberry Pi.
To this end, I want a computer running a full linux kernel with a local C toolchain that also has gpio pins.
I want a system where I can still control everything from my laptop such that there is a wired connection between my laptop and the pi and I can control the pi via this connection.
The Raspberry Pi Zero is a great candidate for this, because of its small form factor, its low cost, and its very low power consumption (it can safely be powered over usb to a laptop because it shouldn't draw more than 2.5A).
The simplest connection I could create would be over the serial port, logging in via ttyACM0.
This requires having 3 pins connected to the Pi's GPIO (TX, RX, GND) and relies on a very simplistic protocol that I do not want to use in the future.


Longterm I would like to use ssh passwordless-login with an ssh key.
I currently have a ReMarkable2 tablet that has been configured to do this, which further motivates me to re-use this paradigm.
This tablet makes use of many busybox utilities to perform this, so it would be beneficial to my reverse engineering effort if I made use of the busybox method on the pi.
Raspbian also has an extraordinarily long boot time on my pi zero, sometimes taking several minutes.
A test with an alpine linux iso revealed this was exclusively a Raspbian issue.
Alpine linux requires network connectivity to setup, and is generally built to be interactive, but I want a system that will be static and embedded, so I will not be using alpine.
Instead, I currently think that my best bet would be to use a buildroot install based on busybox and musl.
Because I am already going to be learning how to use these embedded tools and libraries, I will take advantage of this to use dropbear ssh instead of openssh and other tools built from the ground up for embedded systems when possible.

- buildroot isn't designed to create a system with a local compiler: will i expect to compile locally on the machine, or will i cross-compile from my laptop?
    - if i cross-compile, buildroot will be fine
    - if i want a local compiler, buildroot will be insufficient so i'll need to use [Yocto](https://www.yoctoproject.org/).

## Misc Implementation Ideas

- have the cross compilation infrastructure to compile for AVR chips on the Pi
- use sshfs to automount a directory from the pi onto my laptop so that when it is connected I have access to the code with all of my normal tools
    - or would rsync be better so then the files remain accessible on my laptop when the pi isn't on hand?
