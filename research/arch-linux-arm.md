# Arch Linux ARM

Arch Linux ARM is a popular distribution for arm linux systems, however it does not support the armv6 architecture that the pi zero v1.3 is built on.
Some people have worked on continuing support for armv6, and the forum thread can be found [here](https://archlinuxarm.org/forum/viewtopic.php?t=15783).


## Misc Links

<https://itsfoss.com/install-arch-raspberry-pi/>

