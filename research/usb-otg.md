# USB On The Go

Connecting to the Pi Zero over ssh and usb requires a usb On The Go connection.
I'm currently having issues with this running successfully, which might be due to not having a usb cable that supports usb OTG.

There is a specific mode of USB Gadget mode that makes use of usb otg. 


## Misc Links and unprocessed info

<https://howchoo.com/pi/raspberry-pi-gadget-mode/>

<https://forums.raspberrypi.com/viewtopic.php?t=306121>

<https://glidedigital.com/how-to-tell-if-a-usb-cable-is-for-charging-or-data-transfer>

<https://www.haykranen.nl/2020/11/01/micro-usb-cables-data-power-charge/>

<https://www.lifewire.com/what-is-usb-otg-and-what-does-it-do-4768902>

<https://www.tomshardware.com/reviews/raspberry-pi-headless-setup-how-to,6028.html>

<https://linuxlink.timesys.com/docs/wiki/engineering/HOWTO_Use_USB_Gadget_Ethernet>

<https://linux-sunxi.org/USB_Gadget/Ethernet>

<https://trac.gateworks.com/wiki/linux/OTG>

<https://openwrt.org/docs/guide-user/hardware/usb_gadget>



## USB Ethernet

<https://raspi.tv/2015/ethernet-on-pi-zero-how-to-put-an-ethernet-port-on-your-pi>

<https://oemdrivers.com/network-sr9700-usb-to-ethernet-lan-adapter>

<https://forum.manjaro.org/t/ethernet-adapter-chipset-sr9700-not-working/123726>

<https://unix.stackexchange.com/questions/386162/how-to-set-up-an-usb-ethernet-interface-in-linux>

