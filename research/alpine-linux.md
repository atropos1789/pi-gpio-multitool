# alpine linux

Having such a small footprint and being built on musl and busybox, alpine linux is a good distro to build an embedded system with.
My current struggle is that the alpine install procedure requires a live system, but I would prefer to be able to flash to a microsd card and have a complete system.
Alpine is supposed to be able to support this manner of install, but none of the images prepared for pis support this.
If I can find how these images are prepared, it should be possible for me to create the necessary image.

[Alpine Home Page](https://alpinelinux.org/about/)

## Notes on an attempted install

1. Obtain a *.img file of the alpine armhf image

2. verify the image

3. write the image to a microsd card using dd

4. modify the `/boot/cmdline.txt` file to add the following lines:
```
console=serial0,115200
```
and after `rootwait`:
```
cfg80211.ieee80211_regdom=US
```

5. modify the `/boot/config.txt` file to add the following lines:
```
dtparam=uart0
dtparam=uart0_console
```

This succeeded in booting the system, but it was an entirely in-ram system.
