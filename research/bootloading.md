# Bootloading Embedded Systems

Embedded ARM systems use a different bootloader than MBR/BIOS and GPT/UEFI systems.
The most well known bootloader (as well as the one used in familiar projects such as Arch Linux ARM and Raspbian) is [U-boot](https://docs.u-boot.org/en/latest/build/index.html)
