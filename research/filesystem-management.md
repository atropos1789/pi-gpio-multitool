# Filesystem Management

Given I want to have my toolchains located on the Pi instead of having it only act as a communication mechanism for other devices, I need to decide how I want to access the files on my laptop.
The easiest answer is to just use ssh and whatever text editors are on the pi, but I'd like to be able to use the more complicated development environment present on my laptop, and I'd like to have permanent access to the files even when the pi is not on me.
Direct access to the Pi's storage would be best mediated via sshfs, and this could be configured to automount to a specific location when I connect the pi via USB, but it wouldn't give me offline access to the Pi's resources. 
Maybe there is a way to configure sshfs to sync the r/w data to both drives?
This might be an obstacle to making the Pi portable - would my laptop have issues if there were writes to the pi's storage that weren't done while it was connected to my device?

## SSHFS info

<https://www.digitalocean.com/community/tutorials/how-to-use-sshfs-to-mount-remote-file-systems-over-ssh#step-3-permanently-mounting-the-remote-filesystem>
